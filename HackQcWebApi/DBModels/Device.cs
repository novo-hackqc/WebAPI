﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GeoAPI.Geometries;
using NetTopologySuite.Geometries;

namespace HackQcWebApi.DBModels
{
    public class Device
    {
        public Guid DeviceId { get; set; }
        public Point Position { get; set; }
        public IEnumerable<Measurement> MeasurementHistory { get; set; }
    }
}

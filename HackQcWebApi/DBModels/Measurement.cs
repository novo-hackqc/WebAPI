﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HackQcWebApi.Mqtt;

namespace HackQcWebApi.DBModels
{
    public class Measurement
    {
        public Guid MeasurementId { get; set; }
        public DateTime DateTime { get; set; }
        public float WaterLevel { get; set; }
        public Device Device { get; set; }
    }
}

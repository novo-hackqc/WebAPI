﻿using System;
using System.Collections.Generic;
using NetTopologySuite.Geometries;

namespace HackQcWebApi.DBModels
{
    public class Social
    {
        public Guid id { get; set; }
        public Point Position { get; set; }
        public SocialEventType EventType { get; set; }
        public string Description { get; set; }
        public IList<Comments> Comments { get; set; }
        public int Likes { get; set; }
        public DateTime CreatedDateTime { get; set; }
    }

    public enum SocialEventType
    {
        Roadblock,
        Flood,
        HelpRequest
    }
}

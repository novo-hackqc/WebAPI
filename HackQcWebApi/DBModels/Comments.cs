﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using NetTopologySuite.Geometries;

namespace HackQcWebApi.DBModels
{
    public class Comments
    {
        [Key]
        public Guid CommentId { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedDateTime { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using NetTopologySuite.Geometries;

namespace HackQcWebApi.DBModels
{
    public class FlaqueModel
    {
        [Key]
        public Guid FlaqueId { get; set; }

        public Polygon Polygon { get; set; }
        public bool Active { get; set; }
    }
}

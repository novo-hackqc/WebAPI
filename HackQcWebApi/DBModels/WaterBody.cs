﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NetTopologySuite.Geometries;

namespace HackQcWebApi.DBModels
{
    public class WaterBody
    {
        public long Id { get; set; }
        public int TypeId { get; set; }
        public string TypeDescription { get; set; }
        public Geometry Geometry { get; set; }
    }
}

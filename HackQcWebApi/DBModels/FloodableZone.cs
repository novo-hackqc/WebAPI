﻿using System;
using System.ComponentModel.DataAnnotations;
using NetTopologySuite.Geometries;

namespace HackQcWebApi.DBModels
{
    public class FloodableZone
    {
        [Key]
        public Guid ZoneId { get; set; }
        public FloodableZoneType Type { get; set; }
        public string Description { get; set; }
        public long OriginalId { get; set; }
        public MultiPolygon Geometry { get; set; }
    }

    public enum FloodableZoneType
    {
        None = 0,
        GrandCourant = 1,
        FaibleCourant = 2,
        GrandCourantPont = 3,
        FaibleCourantPont = 4,
        Crue0_2 = 5,
        Crue0_20 = 6,
        Crue0_100 = 7,
        Crue0_2Pont = 8,
        Crue0_20Pont = 9,
        MRC = 10
    }
}
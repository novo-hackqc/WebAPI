﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NetTopologySuite.Geometries;

namespace HackQcWebApi.DBModels
{
    public class TestPuddle
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public NetTopologySuite.Geometries.Polygon Polygon { get; set; }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using NetTopologySuite.Geometries;

namespace HackQcWebApi.DBModels
{
    public class FloodHistory
    {
        public long Id { get; set; }
        public DateTime Datetime { get; set; }
        public Geometry Geometry { get; set; }
    }
}
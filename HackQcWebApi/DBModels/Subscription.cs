﻿using System;
using System.Collections.Generic;
using NetTopologySuite.Geometries;

namespace HackQcWebApi.DBModels
{
    public class Subcription
    {
        public Guid SubcriptionId { get; set; }
        public Point Position { get; set; }
        public string Address { get; set; }
        public string Endpoint { get; set; }
        public string PhoneNumber { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using NetTopologySuite.Geometries;

namespace HackQcWebApi.DBModels
{
    public class Address
    {
        public long Id { get; set; }
        public long fid { get; set; }
        public Point Position { get; set; }
        public string Civic { get; set; }
        public string Street { get; set; }
        public string District { get; set; }
        
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NetTopologySuite.Geometries;

namespace HackQcWebApi.Migrations
{
    public partial class AddDevices : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "TestPuddles",
                keyColumn: "Id",
                keyValue: new Guid("32eee35d-caa5-40f3-a099-57110694529a"));

            migrationBuilder.DeleteData(
                table: "TestPuddles",
                keyColumn: "Id",
                keyValue: new Guid("97d8988c-d19d-4c1b-9833-69f48b32f608"));

            migrationBuilder.CreateTable(
                name: "Devices",
                columns: table => new
                {
                    DeviceId = table.Column<Guid>(nullable: false),
                    Position = table.Column<Point>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Devices", x => x.DeviceId);
                });

            migrationBuilder.CreateTable(
                name: "Measurement",
                columns: table => new
                {
                    MeasurementId = table.Column<Guid>(nullable: false),
                    DateTime = table.Column<DateTime>(nullable: false),
                    WaterLevel = table.Column<float>(nullable: false),
                    DeviceId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Measurement", x => x.MeasurementId);
                    table.ForeignKey(
                        name: "FK_Measurement_Devices_DeviceId",
                        column: x => x.DeviceId,
                        principalTable: "Devices",
                        principalColumn: "DeviceId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "TestPuddles",
                columns: new[] { "Id", "Name", "Polygon" },
                values: new object[,]
                {
                    { new Guid("e6e6d30c-67c6-47e5-8f47-21dc541733b6"), "Grand-Mere", (NetTopologySuite.Geometries.Polygon)new NetTopologySuite.IO.WKTReader().Read("SRID=0;POLYGON ((46.605315 -72.697502, 46.609502 -72.690421, 46.610828 -72.692181, 46.6062 -72.701279, 46.605315 -72.697502))") },
                    { new Guid("15866504-be1e-4670-a392-84e046a9a078"), "Shawinigan", (NetTopologySuite.Geometries.Polygon)new NetTopologySuite.IO.WKTReader().Read("SRID=0;POLYGON ((46.573519 -72.735046, 46.56961 -72.741097, 46.56427 -72.734295, 46.569271 -72.72657, 46.573519 -72.735046))") }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Measurement_DeviceId",
                table: "Measurement",
                column: "DeviceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Measurement");

            migrationBuilder.DropTable(
                name: "Devices");

            migrationBuilder.DeleteData(
                table: "TestPuddles",
                keyColumn: "Id",
                keyValue: new Guid("15866504-be1e-4670-a392-84e046a9a078"));

            migrationBuilder.DeleteData(
                table: "TestPuddles",
                keyColumn: "Id",
                keyValue: new Guid("e6e6d30c-67c6-47e5-8f47-21dc541733b6"));

            migrationBuilder.InsertData(
                table: "TestPuddles",
                columns: new[] { "Id", "Name", "Polygon" },
                values: new object[,]
                {
                    { new Guid("97d8988c-d19d-4c1b-9833-69f48b32f608"), "Grand-Mere", (NetTopologySuite.Geometries.Polygon)new NetTopologySuite.IO.WKTReader().Read("SRID=0;POLYGON ((46.605315 -72.697502, 46.609502 -72.690421, 46.610828 -72.692181, 46.6062 -72.701279, 46.605315 -72.697502))") },
                    { new Guid("32eee35d-caa5-40f3-a099-57110694529a"), "Shawinigan", (NetTopologySuite.Geometries.Polygon)new NetTopologySuite.IO.WKTReader().Read("SRID=0;POLYGON ((46.573519 -72.735046, 46.56961 -72.741097, 46.56427 -72.734295, 46.569271 -72.72657, 46.573519 -72.735046))") }
                });
        }
    }
}

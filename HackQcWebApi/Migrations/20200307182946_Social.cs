﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NetTopologySuite.Geometries;

namespace HackQcWebApi.Migrations
{
    public partial class Social : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "TestPuddles",
                keyColumn: "Id",
                keyValue: new Guid("2c79278c-65c4-43c5-b877-6706322a2297"));

            migrationBuilder.DeleteData(
                table: "TestPuddles",
                keyColumn: "Id",
                keyValue: new Guid("55c3134c-7b87-4a11-b3dd-f2730729aed4"));

            migrationBuilder.CreateTable(
                name: "Social",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    Position = table.Column<Point>(nullable: true),
                    EventType = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Likes = table.Column<int>(nullable: false),
                    CreatedDateTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Social", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    CommentId = table.Column<Guid>(nullable: false),
                    Comment = table.Column<string>(nullable: true),
                    CreatedDateTime = table.Column<DateTime>(nullable: false),
                    Socialid = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.CommentId);
                    table.ForeignKey(
                        name: "FK_Comments_Social_Socialid",
                        column: x => x.Socialid,
                        principalTable: "Social",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "TestPuddles",
                columns: new[] { "Id", "Name", "Polygon" },
                values: new object[,]
                {
                    { new Guid("e4136818-745d-48ce-93ed-d25b67680290"), "Grand-Mere", (NetTopologySuite.Geometries.Polygon)new NetTopologySuite.IO.WKTReader().Read("SRID=0;POLYGON ((46.605315 -72.697502, 46.609502 -72.690421, 46.610828 -72.692181, 46.6062 -72.701279, 46.605315 -72.697502))") },
                    { new Guid("f7330e07-4b69-4ec8-ad41-7efdfba9963f"), "Shawinigan", (NetTopologySuite.Geometries.Polygon)new NetTopologySuite.IO.WKTReader().Read("SRID=0;POLYGON ((46.573519 -72.735046, 46.56961 -72.741097, 46.56427 -72.734295, 46.569271 -72.72657, 46.573519 -72.735046))") }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Comments_Socialid",
                table: "Comments",
                column: "Socialid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "Social");

            migrationBuilder.DeleteData(
                table: "TestPuddles",
                keyColumn: "Id",
                keyValue: new Guid("e4136818-745d-48ce-93ed-d25b67680290"));

            migrationBuilder.DeleteData(
                table: "TestPuddles",
                keyColumn: "Id",
                keyValue: new Guid("f7330e07-4b69-4ec8-ad41-7efdfba9963f"));

            migrationBuilder.InsertData(
                table: "TestPuddles",
                columns: new[] { "Id", "Name", "Polygon" },
                values: new object[,]
                {
                    { new Guid("2c79278c-65c4-43c5-b877-6706322a2297"), "Grand-Mere", (NetTopologySuite.Geometries.Polygon)new NetTopologySuite.IO.WKTReader().Read("SRID=0;POLYGON ((46.605315 -72.697502, 46.609502 -72.690421, 46.610828 -72.692181, 46.6062 -72.701279, 46.605315 -72.697502))") },
                    { new Guid("55c3134c-7b87-4a11-b3dd-f2730729aed4"), "Shawinigan", (NetTopologySuite.Geometries.Polygon)new NetTopologySuite.IO.WKTReader().Read("SRID=0;POLYGON ((46.573519 -72.735046, 46.56961 -72.741097, 46.56427 -72.734295, 46.569271 -72.72657, 46.573519 -72.735046))") }
                });
        }
    }
}

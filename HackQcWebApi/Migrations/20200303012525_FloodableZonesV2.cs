﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NetTopologySuite.Geometries;

namespace HackQcWebApi.Migrations
{
    public partial class FloodableZonesV2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "TestPuddles",
                keyColumn: "Id",
                keyValue: new Guid("232215ba-ed58-4b5a-935b-1ab15445a540"));

            migrationBuilder.DeleteData(
                table: "TestPuddles",
                keyColumn: "Id",
                keyValue: new Guid("c30e789a-0de5-4d81-9051-a81a50813049"));

            migrationBuilder.AlterColumn<long>(
                name: "OriginalId",
                table: "FloodableZones",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.InsertData(
                table: "TestPuddles",
                columns: new[] { "Id", "Name", "Polygon" },
                values: new object[,]
                {
                    { new Guid("a3365075-0cf1-46c2-813c-73e39f2cefdc"), "Grand-Mere", (NetTopologySuite.Geometries.Polygon)new NetTopologySuite.IO.WKTReader().Read("SRID=0;POLYGON ((46.605315 -72.697502, 46.609502 -72.690421, 46.610828 -72.692181, 46.6062 -72.701279, 46.605315 -72.697502))") },
                    { new Guid("f1562a4f-f4df-4154-8352-84d39a4533c6"), "Shawinigan", (NetTopologySuite.Geometries.Polygon)new NetTopologySuite.IO.WKTReader().Read("SRID=0;POLYGON ((46.573519 -72.735046, 46.56961 -72.741097, 46.56427 -72.734295, 46.569271 -72.72657, 46.573519 -72.735046))") }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "TestPuddles",
                keyColumn: "Id",
                keyValue: new Guid("a3365075-0cf1-46c2-813c-73e39f2cefdc"));

            migrationBuilder.DeleteData(
                table: "TestPuddles",
                keyColumn: "Id",
                keyValue: new Guid("f1562a4f-f4df-4154-8352-84d39a4533c6"));

            migrationBuilder.AlterColumn<int>(
                name: "OriginalId",
                table: "FloodableZones",
                type: "integer",
                nullable: false,
                oldClrType: typeof(long));

            migrationBuilder.InsertData(
                table: "TestPuddles",
                columns: new[] { "Id", "Name", "Polygon" },
                values: new object[,]
                {
                    { new Guid("232215ba-ed58-4b5a-935b-1ab15445a540"), "Grand-Mere", (NetTopologySuite.Geometries.Polygon)new NetTopologySuite.IO.WKTReader().Read("SRID=0;POLYGON ((46.605315 -72.697502, 46.609502 -72.690421, 46.610828 -72.692181, 46.6062 -72.701279, 46.605315 -72.697502))") },
                    { new Guid("c30e789a-0de5-4d81-9051-a81a50813049"), "Shawinigan", (NetTopologySuite.Geometries.Polygon)new NetTopologySuite.IO.WKTReader().Read("SRID=0;POLYGON ((46.573519 -72.735046, 46.56961 -72.741097, 46.56427 -72.734295, 46.569271 -72.72657, 46.573519 -72.735046))") }
                });
        }
    }
}

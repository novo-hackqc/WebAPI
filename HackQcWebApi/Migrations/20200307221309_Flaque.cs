﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NetTopologySuite.Geometries;

namespace HackQcWebApi.Migrations
{
    public partial class Flaque : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TestPuddles");

            migrationBuilder.CreateTable(
                name: "Flaques",
                columns: table => new
                {
                    FlaqueId = table.Column<Guid>(nullable: false),
                    Polygon = table.Column<Polygon>(nullable: true),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Flaques", x => x.FlaqueId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Flaques");

            migrationBuilder.CreateTable(
                name: "TestPuddles",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Polygon = table.Column<Polygon>(type: "geometry", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TestPuddles", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "TestPuddles",
                columns: new[] { "Id", "Name", "Polygon" },
                values: new object[,]
                {
                    { new Guid("16c83955-68e1-41a4-87b7-665a74140e0d"), "Grand-Mere", (NetTopologySuite.Geometries.Polygon)new NetTopologySuite.IO.WKTReader().Read("SRID=0;POLYGON ((46.605315 -72.697502, 46.609502 -72.690421, 46.610828 -72.692181, 46.6062 -72.701279, 46.605315 -72.697502))") },
                    { new Guid("a7c47396-64b9-40dc-be10-a9b503e2b430"), "Shawinigan", (NetTopologySuite.Geometries.Polygon)new NetTopologySuite.IO.WKTReader().Read("SRID=0;POLYGON ((46.573519 -72.735046, 46.56961 -72.741097, 46.56427 -72.734295, 46.569271 -72.72657, 46.573519 -72.735046))") }
                });
        }
    }
}

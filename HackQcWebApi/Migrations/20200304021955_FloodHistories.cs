﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NetTopologySuite.Geometries;

namespace HackQcWebApi.Migrations
{
    public partial class FloodHistories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "TestPuddles",
                keyColumn: "Id",
                keyValue: new Guid("a3365075-0cf1-46c2-813c-73e39f2cefdc"));

            migrationBuilder.DeleteData(
                table: "TestPuddles",
                keyColumn: "Id",
                keyValue: new Guid("f1562a4f-f4df-4154-8352-84d39a4533c6"));

            migrationBuilder.CreateTable(
                name: "FloodHistories",
                columns: table => new
                {
                    FloodHistoryId = table.Column<Guid>(nullable: false),
                    OriginalId = table.Column<long>(nullable: false),
                    Year = table.Column<int>(nullable: false),
                    Geometry = table.Column<Polygon>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FloodHistories", x => x.FloodHistoryId);
                });

            migrationBuilder.InsertData(
                table: "TestPuddles",
                columns: new[] { "Id", "Name", "Polygon" },
                values: new object[,]
                {
                    { new Guid("74024852-ff17-4375-8974-539f85eb9b54"), "Grand-Mere", (NetTopologySuite.Geometries.Polygon)new NetTopologySuite.IO.WKTReader().Read("SRID=0;POLYGON ((46.605315 -72.697502, 46.609502 -72.690421, 46.610828 -72.692181, 46.6062 -72.701279, 46.605315 -72.697502))") },
                    { new Guid("29a04131-a579-45e1-bbec-7899f5d943f2"), "Shawinigan", (NetTopologySuite.Geometries.Polygon)new NetTopologySuite.IO.WKTReader().Read("SRID=0;POLYGON ((46.573519 -72.735046, 46.56961 -72.741097, 46.56427 -72.734295, 46.569271 -72.72657, 46.573519 -72.735046))") }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FloodHistories");

            migrationBuilder.DeleteData(
                table: "TestPuddles",
                keyColumn: "Id",
                keyValue: new Guid("29a04131-a579-45e1-bbec-7899f5d943f2"));

            migrationBuilder.DeleteData(
                table: "TestPuddles",
                keyColumn: "Id",
                keyValue: new Guid("74024852-ff17-4375-8974-539f85eb9b54"));

            migrationBuilder.InsertData(
                table: "TestPuddles",
                columns: new[] { "Id", "Name", "Polygon" },
                values: new object[,]
                {
                    { new Guid("a3365075-0cf1-46c2-813c-73e39f2cefdc"), "Grand-Mere", (NetTopologySuite.Geometries.Polygon)new NetTopologySuite.IO.WKTReader().Read("SRID=0;POLYGON ((46.605315 -72.697502, 46.609502 -72.690421, 46.610828 -72.692181, 46.6062 -72.701279, 46.605315 -72.697502))") },
                    { new Guid("f1562a4f-f4df-4154-8352-84d39a4533c6"), "Shawinigan", (NetTopologySuite.Geometries.Polygon)new NetTopologySuite.IO.WKTReader().Read("SRID=0;POLYGON ((46.573519 -72.735046, 46.56961 -72.741097, 46.56427 -72.734295, 46.569271 -72.72657, 46.573519 -72.735046))") }
                });
        }
    }
}

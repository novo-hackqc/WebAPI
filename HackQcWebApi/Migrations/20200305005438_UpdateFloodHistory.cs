﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NetTopologySuite.Geometries;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace HackQcWebApi.Migrations
{
    public partial class UpdateFloodHistory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_FloodHistories",
                table: "FloodHistories");

            migrationBuilder.DeleteData(
                table: "TestPuddles",
                keyColumn: "Id",
                keyValue: new Guid("29809d57-78e1-4c1c-a7be-fd90d7b75843"));

            migrationBuilder.DeleteData(
                table: "TestPuddles",
                keyColumn: "Id",
                keyValue: new Guid("46259630-813c-4493-9b8c-01caf50a634c"));

            migrationBuilder.DropColumn(
                name: "FloodHistoryId",
                table: "FloodHistories");

            migrationBuilder.DropColumn(
                name: "OriginalId",
                table: "FloodHistories");

            migrationBuilder.DropColumn(
                name: "Year",
                table: "FloodHistories");

            migrationBuilder.AddColumn<long>(
                name: "Id",
                table: "FloodHistories",
                nullable: false,
                defaultValue: 0L)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.AddColumn<DateTime>(
                name: "Datetime",
                table: "FloodHistories",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddPrimaryKey(
                name: "PK_FloodHistories",
                table: "FloodHistories",
                column: "Id");

            migrationBuilder.InsertData(
                table: "TestPuddles",
                columns: new[] { "Id", "Name", "Polygon" },
                values: new object[,]
                {
                    { new Guid("d5f0ce20-627f-4949-944a-92d34b8a1c82"), "Grand-Mere", (NetTopologySuite.Geometries.Polygon)new NetTopologySuite.IO.WKTReader().Read("SRID=0;POLYGON ((46.605315 -72.697502, 46.609502 -72.690421, 46.610828 -72.692181, 46.6062 -72.701279, 46.605315 -72.697502))") },
                    { new Guid("4a458eb3-42bd-44ab-b2a8-7c60001c6731"), "Shawinigan", (NetTopologySuite.Geometries.Polygon)new NetTopologySuite.IO.WKTReader().Read("SRID=0;POLYGON ((46.573519 -72.735046, 46.56961 -72.741097, 46.56427 -72.734295, 46.569271 -72.72657, 46.573519 -72.735046))") }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_FloodHistories",
                table: "FloodHistories");

            migrationBuilder.DeleteData(
                table: "TestPuddles",
                keyColumn: "Id",
                keyValue: new Guid("4a458eb3-42bd-44ab-b2a8-7c60001c6731"));

            migrationBuilder.DeleteData(
                table: "TestPuddles",
                keyColumn: "Id",
                keyValue: new Guid("d5f0ce20-627f-4949-944a-92d34b8a1c82"));

            migrationBuilder.DropColumn(
                name: "Id",
                table: "FloodHistories");

            migrationBuilder.DropColumn(
                name: "Datetime",
                table: "FloodHistories");

            migrationBuilder.AddColumn<Guid>(
                name: "FloodHistoryId",
                table: "FloodHistories",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<long>(
                name: "OriginalId",
                table: "FloodHistories",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<int>(
                name: "Year",
                table: "FloodHistories",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_FloodHistories",
                table: "FloodHistories",
                column: "FloodHistoryId");

            migrationBuilder.InsertData(
                table: "TestPuddles",
                columns: new[] { "Id", "Name", "Polygon" },
                values: new object[,]
                {
                    { new Guid("46259630-813c-4493-9b8c-01caf50a634c"), "Grand-Mere", (NetTopologySuite.Geometries.Polygon)new NetTopologySuite.IO.WKTReader().Read("SRID=0;POLYGON ((46.605315 -72.697502, 46.609502 -72.690421, 46.610828 -72.692181, 46.6062 -72.701279, 46.605315 -72.697502))") },
                    { new Guid("29809d57-78e1-4c1c-a7be-fd90d7b75843"), "Shawinigan", (NetTopologySuite.Geometries.Polygon)new NetTopologySuite.IO.WKTReader().Read("SRID=0;POLYGON ((46.573519 -72.735046, 46.56961 -72.741097, 46.56427 -72.734295, 46.569271 -72.72657, 46.573519 -72.735046))") }
                });
        }
    }
}

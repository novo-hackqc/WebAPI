﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HackQcWebApi.Data;
using HackQcWebApi.DBModels;
using Microsoft.EntityFrameworkCore;
using NetTopologySuite.Geometries;
using Newtonsoft.Json;

namespace HackQcWebApi.Ingestion
{
    public class IngestionManager
    {
        private DatabaseContext _dbContext;

        public IngestionManager(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        /*public void IngestMrc()
        {
            var geoFactory = new GeometryFactory(new PrecisionModel(), 4326);

            var reader = new NetTopologySuite.IO.GeoJsonReader(geoFactory, new JsonSerializerSettings());
            string text = File.ReadAllText("Ingestion/mrc.geojson", Encoding.UTF8);
            var features = reader.Read<NetTopologySuite.Features.FeatureCollection>(text);

            foreach (var feature in features)
            {
                var originalId = (long) feature.Attributes.GetOptionalValue("objectid");


                var zone = _dbContext.FloodableZones.FirstOrDefault(z => (z.OriginalId == originalId && z.Type == FloodableZoneType.MRC));
                if (zone == null)
                {
                    zone = new FloodableZone
                    {
                        OriginalId = originalId,
                        Description = (string)feature.Attributes.GetOptionalValue("descriptio"),
                        Type = FloodableZoneType.MRC
                    };
                }
                
                zone.Geometry = feature.Geometry.GeometryType switch
                {
                    "Polygon" => geoFactory.CreateMultiPolygon(new [] {geoFactory.CreatePolygon((Polygon)feature.Geometry.Coordinates)}),
                    "MultiPolygon" => (Polygon)feature.Geometry,
                    _ => throw new Exception("Unsuported geometry type"),
                };

                _dbContext.FloodableZones.Update(zone);

                //Prevent server from keeping every entity in RAM
                _dbContext.SaveChanges();
                _dbContext.Entry(zone).State = EntityState.Detached;
            }
        }

        public void IngestZoi()
        {
            var geoFactory = new GeometryFactory(new PrecisionModel(), 4326);

            var reader = new NetTopologySuite.IO.GeoJsonReader(geoFactory, new JsonSerializerSettings());
            string text = File.ReadAllText("Ingestion/zoi.geojson", Encoding.UTF8);
            var features = reader.Read<NetTopologySuite.Features.FeatureCollection>(text);

            var invalidCount = 0;

            foreach (var feature in features)
            {
                var originalId = Int64.Parse((string)feature.Attributes.GetOptionalValue("Id_uni_Zoi"));


                var zone = _dbContext.FloodableZones.FirstOrDefault(z => (z.OriginalId == originalId && z.Type != FloodableZoneType.MRC));
                var type = (string) feature.Attributes.GetOptionalValue("Indicatif");
                if (zone == null)
                {
                    zone = new FloodableZone
                    {
                        OriginalId = originalId,
                        Description = (string)feature.Attributes.GetOptionalValue("Description"),
                        Type = (FloodableZoneType)Int16.Parse(type.Substring(9))
                    };
                }

                zone.Geometry = feature.Geometry.GeometryType switch
                {
                    "Polygon" => geoFactory.CreateMultiPolygon(new[] { geoFactory.CreatePolygon(feature.Geometry.Coordinates) }),
                    "MultiPolygon" => (Polygon)feature.Geometry,
                    _ => throw new Exception("Unsuported geometry type"),
                };

                if (zone.Geometry.IsValid)
                {
                    _dbContext.FloodableZones.Update(zone);
                    //Prevent server from keeping every entity in RAM
                    _dbContext.SaveChanges();
                    _dbContext.Entry(zone).State = EntityState.Detached;
                }
                else
                {
                    invalidCount++;
                }
            }
        }*/

        /*public void IngestHist2019()
        {
            var geoFactory = new GeometryFactory(new PrecisionModel(), 4326);

            var reader = new NetTopologySuite.IO.GeoJsonReader(geoFactory, new JsonSerializerSettings());
            string text = File.ReadAllText("Ingestion/hist2019.geojson", Encoding.UTF8);
            var features = reader.Read<NetTopologySuite.Features.FeatureCollection>(text);

            var invalidCount = 0;

            foreach (var feature in features)
            {
                var originalId = (long)feature.Attributes.GetOptionalValue("OBJECTID");


                var history = _dbContext.FloodHistories.FirstOrDefault(z => (z.OriginalId == originalId));
                if (history == null)
                {
                    history = new FloodHistory
                    {
                        OriginalId = originalId,
                        Year = 2019
                    };
                }

                try
                {
                    history.Geometry = feature.Geometry.GeometryType switch
                    {
                        "Polygon" => geoFactory.CreateMultiPolygon(new[]
                            {geoFactory.CreatePolygon(feature.Geometry.Coordinates)}),
                        "MultiPolygon" => (MultiPolygon) feature.Geometry,
                        _ => throw new Exception("Unsuported geometry type"),
                    };

                    if (history.Geometry.IsValid)
                    {
                        _dbContext.FloodHistories.Update(history);
                        //Prevent server from keeping every entity in RAM
                        _dbContext.SaveChanges();
                        _dbContext.Entry(history).State = EntityState.Detached;
                    }
                    else
                    {
                        invalidCount++;
                    }
                }
                catch (Exception e)
                {
                    invalidCount++;
                }
            }
        }*/
    }
}

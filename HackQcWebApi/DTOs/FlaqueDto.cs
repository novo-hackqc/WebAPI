﻿using System.Collections.Generic;
using flaque;

namespace HackQcWebApi.DTOs
{
    public class FlaqueDto
    {
        public List<Location> Locs { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HackQcWebApi.DBModels;
using NetTopologySuite.GeometriesGraph;

namespace HackQcWebApi.DTOs
{
    public class DeviceDto
    {
        public Guid DeviceId { get; set; }
        public CoordinateDto Position { get; set; }
        public DeviceStatus Status { get; set; }
        public float CurrentWaterLevel { get; set; }
        public IEnumerable<MeasurementDto> MeasurementHistory { get; set; }

        public DeviceDto(Device model)
        {
            DeviceId = model.DeviceId;
            Position = new CoordinateDto(model.Position.X, model.Position.Y);
            Status = DeviceStatus.Active;
            CurrentWaterLevel = model.MeasurementHistory.OrderByDescending(m => m.DateTime).First().WaterLevel;
            MeasurementHistory = model.MeasurementHistory.Select(m => new MeasurementDto
            {
                DateTime = m.DateTime,
                WaterLevel = m.WaterLevel
            });
        }
    }
}

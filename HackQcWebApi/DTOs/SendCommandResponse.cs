﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HackQcWebApi.DTOs
{
    public class SendCommandResponse
    {
        public Guid DeviceId { get; set; }
        public Guid CommandId { get; set; }
        public bool Success { get; set; }
    }
}

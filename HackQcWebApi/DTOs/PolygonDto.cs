﻿using System.Collections.Generic;

namespace HackQcWebApi.DTOs
{
    public class PolygonDto
    {
        public IEnumerable<CoordinateDto> Coordinates { get; set; }  
    }
}
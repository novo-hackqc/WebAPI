﻿using System.Collections.Generic;
using HackQcWebApi.DBModels;

namespace HackQcWebApi.DTOs
{
    public class AddressInfoDto
    {
        public FloodableZoneType FloodableZoneType { get; set; }
        public string FloodableZoneDescription { get; set; }
        public IEnumerable<int> FloodedYears { get; set; }
        public double CurrentWaterLevel { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HackQcWebApi.DTOs
{
    public class RefreshPositionCommandDto
    {
        public Guid DeviceId { get; set; }
    }
}

﻿namespace HackQcWebApi.DTOs
{
    public class DashboardResponseDto
    {
        public bool Success { get; set; }
    }

    public class SendSmsDto
    {
        public CoordinateDto Coordinates { get; set; }
        public double Radius { get; set; }
        public string Text { get; set; }
    }

    public class StatsDto
    {
        public int DevicesCount { get; set; }
        public int FloodedAddresses { get; set; }
        public int FloodedPeople { get; set; }
        public int PeopleRequestingHelp { get; set; }
        public double FloodedArea { get; set; }
    }
}
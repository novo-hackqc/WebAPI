﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;

namespace HackQcWebApi.DTOs
{
    public enum SocialEventType
    {
        Roadblock,
        Flood,
        HelpRequest
    }

    public class SocialCommandResponseDto
    {
        public bool Success { get; set; }
        public Guid guid { get; set; }
    }

    public class AddSocialEventDto
    {
        public SocialEventType EventType { get; set; }
        public CoordinateDto Coordinates { get; set; }
        public string Description { get; set; }
        public IEnumerable<byte> Image { get; set; }
    }

    public class SocialEventDto
    {
        public Guid EventId { get; set; }
        public SocialEventType EventType { get; set; }
        public CoordinateDto Coordinates { get; set; }
        public string Description { get; set; }
        public IEnumerable<byte> Image { get; set; }
        public IEnumerable<CommentDto> Comments { get; set; }
        public int Likes { get; set; }
        public int Dislikes { get; set; }
        public DateTime CreatedDateTime { get; set; }
    }

    public class AddCommentDto
    {
        public string Comment { get; set; }
    }

    public class CommentDto
    {
        public Guid CommentId { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedDateTime { get; set; }

    }
}

﻿namespace HackQcWebApi.DTOs
{
    public class ZoneTypeDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
﻿namespace HackQcWebApi.DTOs
{
    public class FloodHistoryDto
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
        public double Zoom { get; set; }
        public int Year { get; set; }

    }
}
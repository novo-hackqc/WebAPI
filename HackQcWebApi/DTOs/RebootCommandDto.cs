﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HackQcWebApi.DTOs
{
    public class RebootCommandDto
    {
        public Guid DeviceId { get; set; }
        public int RebootTimeSec { get; set; }
    }
}

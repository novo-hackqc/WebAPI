﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HackQcWebApi.DBModels;
using NetTopologySuite.Geometries;

namespace HackQcWebApi.DTOs
{
    public class PuddleDto
    {
        public float WaterLevel { get; set; }
        public int EnclosedAddresses { get; set; }
        public IEnumerable<CoordinateDto> Outline { get; set; }
        public IEnumerable<IEnumerable<CoordinateDto>> Holes { get; set; }

        /*public PuddleDto(TestPuddle model)
        {
            Name = model.Name;
            Coordinates = model.Polygon.Boundary.Coordinates.Select(
                c => new CoordinateDto(c.X,c.Y))
                .ToList();
        }*/
    }
}

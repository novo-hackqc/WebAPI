﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HackQcWebApi.DTOs
{
    public class MeasurementDto
    {
        public float WaterLevel { get; set; }
        public DateTime DateTime { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HackQcWebApi.DTOs
{
    public class CoordinateDto
    {
        public double Lat { get; set; }
        public double Lng { get; set; }

        public CoordinateDto() { }

        public CoordinateDto(double lng, double lat)
        {
            Lat = lat;
            Lng = lng;
        }
    }
}

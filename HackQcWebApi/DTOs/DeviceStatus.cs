﻿namespace HackQcWebApi.DTOs
{
    public enum DeviceStatus
    {
        Active,
        NotResponding
    }
}
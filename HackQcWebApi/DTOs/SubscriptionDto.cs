﻿using System;
using System.Collections.Generic;

namespace HackQcWebApi.DTOs
{
    public class SubscriptionDto
    {
        public AddressDto Address { get; set; }
        public string PhoneNumber { get; set; }
    }

    public class AddressDto
    {
        public string Description { get; set; }
        public CoordinateDto Coordinates { get; set; }
    }

    public class SubscriptionResultDto
    {
        public bool Success { get; set; }
    }
}
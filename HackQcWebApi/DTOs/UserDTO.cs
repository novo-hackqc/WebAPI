﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HackQcWebApi.DBModels;
using NetTopologySuite.Geometries;
using NetTopologySuite.Operation.Overlay;

namespace HackQcWebApi.DTOs
{
    public class UserDto
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public UserDto() { }

        public UserDto(User model)
        {
            Email = model.Email;
            FirstName = model.FirstName;
            LastName = model.LastName;
        }

        public User ToModel()
        {
            return new User
            {
                Email = Email,
                FirstName = FirstName,
                LastName = LastName
            };
        }
    }
}

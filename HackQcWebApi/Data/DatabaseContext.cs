﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using HackQcWebApi.DBModels;
using NetTopologySuite.Geometries;
using NetTopologySuite.GeometriesGraph;
using NetTopologySuite.Operation.Overlay;

namespace HackQcWebApi.Data
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext (DbContextOptions<DatabaseContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("postgis");

            var geoFactory = new GeometryFactory();

            modelBuilder.Entity<WaterBody>()
                .HasIndex(w => w.Geometry)
                .HasMethod("gist");
        }

        public DbSet<User> User { get; set; }
        public DbSet<FlaqueModel> Flaques { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<Measurement> Measurements { get; set; }
        public DbSet<FloodableZone> FloodableZones { get; set; }
        public DbSet<FloodHistory> FloodHistories { get; set; }
        public DbSet<WaterBody> WaterBodies { get; set; }
        public DbSet<Subcription> Subcription { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Social> Social { get; set; }
    }
}

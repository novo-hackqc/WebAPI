﻿using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using HackQcWebApi.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackQcWebApi.Services
{
    public class SNS
    {
        private DatabaseContext _dbContext;
        private AmazonSimpleNotificationServiceClient client;

        public SNS(DatabaseContext databaseContext)
        {
            _dbContext = databaseContext;

            var awsCredentials = new Amazon.Runtime.BasicAWSCredentials("AKIA266RSFCXCCAT637K", "ezHqg+e6+eoa1nQrJ4CzbzcmMBlh7uCEYVF2xT9j");
            client = new Amazon.SimpleNotificationService.AmazonSimpleNotificationSer‌​viceClient(
                                          awsCreden‌​tials, Amazon.RegionEndpoint.USEast1);
        }
        public async Task<string> CreateTopicSubscribe(Guid TopicID, string DisplayName, string PhoneNumber)
        {
            // Create an Amazon SNS topic.
            CreateTopicRequest createTopicRequest = new CreateTopicRequest(TopicID.ToString());
            CreateTopicResponse createTopicResponse = await client.CreateTopicAsync(createTopicRequest);

            var SB = new StringBuilder().Append("Alerte Inondation ").Append(DisplayName);
            var Name = SB.ToString();
            await client.SetTopicAttributesAsync(createTopicResponse.TopicArn, "DisplayName", Name);

            // Print the topic ARN.
            Console.WriteLine("TopicArn: " + createTopicResponse.TopicArn);

            // Subscribe an SMS endpoint to an Amazon SNS topic.
            SubscribeRequest subscribeRequest = new SubscribeRequest(createTopicResponse.TopicArn, "sms", PhoneNumber);
            SubscribeResponse subscribeResponse = await client.SubscribeAsync(subscribeRequest);

            return createTopicResponse.TopicArn;
        }

        public async Task<bool> SendSMS(string TopicArn, string msg)
        {
            PublishRequest publishRequest = new PublishRequest(TopicArn, msg);
            PublishResponse publishResponse = await client.PublishAsync(publishRequest);

            return true;
        }
    }
}

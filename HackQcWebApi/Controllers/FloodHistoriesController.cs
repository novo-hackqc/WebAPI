﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HackQcWebApi.Data;
using HackQcWebApi.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using NetTopologySuite.Geometries;

namespace HackQcWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FloodHistoriesController : ControllerBase
    {
        private DatabaseContext _dbContext;

        public FloodHistoriesController(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpPost]
        public IEnumerable<PolygonDto> GetFloodHistory(FloodHistoryDto request)
        {
            var geoFactory = new GeometryFactory(new PrecisionModel(), 4326);

            var history = _dbContext.FloodHistories
                .Where(h =>
                    h.Datetime.Year == request.Year &&
                    h.Geometry.IsWithinDistance(geoFactory.CreatePoint(new Coordinate(request.Lng, request.Lat)),
                        request.Zoom));

            var returnValue = history.Select(h => new PolygonDto {Coordinates = h.Geometry.Coordinates.Select(c => new CoordinateDto(c.X, c.Y))}).ToList();

            return returnValue;
        }
    }
}
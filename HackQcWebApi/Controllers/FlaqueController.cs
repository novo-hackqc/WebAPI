﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HackQcWebApi.Data;
using HackQcWebApi.DBModels;
using HackQcWebApi.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NetTopologySuite.Geometries;
using Location = flaque.Location;
using Polygon = flaque.Polygon;

namespace HackQcWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FlaqueController : ControllerBase
    {

        private DatabaseContext _dbContext;
        private Flaque flaque;

        public FlaqueController(DatabaseContext dbContext, Flaque flaque)
        {
            _dbContext = dbContext;
            this.flaque = flaque;
        }

        //Params temporaire juste pour tester
        [HttpGet()]
        public async Task<List<FlaqueDto>> getFlaqueAsync()
        {
            var models = _dbContext.Devices
              .Select(d => new Device
              {
                  DeviceId = d.DeviceId,
                  Position = d.Position,
                  MeasurementHistory = d.MeasurementHistory.OrderByDescending(m => m.DateTime).Take(1)
              })
              .ToList();

            List<List<Location>> polys = new List<List<Location>>();

            foreach (Device d in models)
            {
                Location loc = new Location(d.Position.Y, d.Position.X);
                double waterLevel = 0;
                foreach (Measurement m in d.MeasurementHistory)
                {
                    waterLevel = m.WaterLevel;

                }

                var flaque2 = await flaque.GenFlaque(loc, waterLevel);

                if (flaque2 != null)
                {
                    var smoothed = flaque2.smooth(3, 0.5);
                    polys.Add(smoothed.mainPoly);
                }
            }

            var oldFlaques = _dbContext.Flaques.Where(f => f.Active == true).ToList();
            oldFlaques.ForEach(f => { f.Active = false; });

            _dbContext.SaveChanges();

            var geoFactory = new GeometryFactory(new PrecisionModel(), 4326);
            foreach (var flaque in polys)
            {
                var points = flaque.Select(c => (new Coordinate(c.lng, c.lat)));
                points = points.Append(new Coordinate(flaque[0].lng, flaque[0].lat));

                _dbContext.Flaques.Add(new FlaqueModel()
                {
                    Active = true,
                    Polygon = geoFactory.CreatePolygon(points.ToArray())
                });
            }

            _dbContext.SaveChanges();



            // Polygon poly = await flaque.GenFlaque(new Location(45.521758, -73.860316), waterLevel); //laval


            //Polygon poly = await flaque.GenFlaque(new Location(46.971845, -70.787696), waterLevel); //ile madame

            //Polygon poly = await flaque.GenFlaque(new Location(46.356438, -72.509606), waterLevel); //3ri ile
            // Polygon poly = await flaque.GenFlaque(new Location(46.346166, -72.530558), waterLevel); //trois ri centro
            //Polygon poly = await flaque.GenFlaque(new Location(46.331487, -72.517100), waterLevel);
            // Polygon poly = await flaque.GenFlaque(new Location(lat, lng), waterLevel);
            //poly.enlargePoints(1f);
            var returnValue = new List<FlaqueDto>();
            foreach (var poly in polys)
            {
                returnValue.Add( new FlaqueDto
                {
                    Locs = poly
                });
            }
            return returnValue;
        }

        //Params temporaire juste pour tester
        [HttpGet("test")]
        public async Task<List<Location>> getFlaqueTestAsync([Required] double lat, [Required] double lng, [Required] double waterLevel)
        {

            Polygon poly = await flaque.GenFlaque(new Location(45.521758, -73.860316), waterLevel); //laval


            //Polygon poly = await flaque.GenFlaque(new Location(46.971845, -70.787696), waterLevel); //ile madame

            //Polygon poly = await flaque.GenFlaque(new Location(46.356438, -72.509606), waterLevel); //3ri ile
            // Polygon poly = await flaque.GenFlaque(new Location(46.346166, -72.530558), waterLevel); //trois ri centro
            //Polygon poly = await flaque.GenFlaque(new Location(46.331487, -72.517100), waterLevel);
            // Polygon poly = await flaque.GenFlaque(new Location(lat, lng), waterLevel);
            //poly.enlargePoints(1f);
            return poly.smooth(3, 0.5).mainPoly;
        }
        /*

        //Params temporaire juste pour tester
        [HttpGet("polygon")]
        public async Task<Polygon> getFlaquePolygonAsync([Required] double lat, [Required] double lng, [Required] double waterLevel)
        {
            Polygon poly = await flaque.GenFlaque(new Location(46.356438, -72.509606), waterLevel);
            
           // Polygon poly = await flaque.GenFlaque(new Location(lat, lng), waterLevel);
            return poly;
        }

        //Params temporaire juste pour tester
        [HttpGet("heatmap")]
        public async Task<Polygon> getFlaqueHeatMapAsync([Required] double lat, [Required] double lng, [Required] double waterLevel)
        {
            Polygon poly = await flaque.GenFlaque(new Location(46.356438, -72.509606), 0.05);

            // Polygon poly = await flaque.GenFlaque(new Location(lat, lng), waterLevel);
            return poly;
        }*/
    }
}
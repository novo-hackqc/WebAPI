﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HackQcWebApi.Data;
using HackQcWebApi.DBModels;
using HackQcWebApi.DTOs;
using HackQcWebApi.Mqtt;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NetTopologySuite.Geometries;

namespace HackQcWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicesController : ControllerBase
    {
        private MqttManager _mqttManager;
        private DatabaseContext _dbContext;

        public DevicesController(MqttManager mqttManager, DatabaseContext dbContext)
        {
            _mqttManager = mqttManager;
            _dbContext = dbContext;
        }

        private NetTopologySuite.Geometries.Point CreateCoord(double y, double x)
        {
            var geoFactory = new GeometryFactory(new PrecisionModel(), 4326);
            return geoFactory.CreatePoint(new Coordinate(x, y));
        }

        [HttpGet]
        public IEnumerable<DeviceDto> GetDevicesInRange([Required]double lat, [Required]double lng, [Required]double range, DeviceStatus? status, int maxHistory = 5)
        {
            var models = _dbContext.Devices
                .Where(d => d.Position.IsWithinDistance(new Point(lng, lat), range))
                .Select(d => new Device
                {
                    DeviceId = d.DeviceId,
                    Position = d.Position,
                    MeasurementHistory = d.MeasurementHistory.OrderByDescending(m => m.DateTime).Take(maxHistory)
                })
                .ToList();

            return models.Select(m => new DeviceDto(m));
        }

        [HttpGet("All")]
        public IEnumerable<DeviceDto> GetDevicesInRange(int maxHistory = 5)
        {
            var models = _dbContext.Devices
                .Select(d => new Device
                {
                    DeviceId = d.DeviceId,
                    Position = d.Position,
                    MeasurementHistory = d.MeasurementHistory.OrderByDescending(m => m.DateTime).Take(maxHistory)
                })
                .ToList();

            return models.Select(m => new DeviceDto(m));
        }

        [HttpPost("addMeasure")]
        public void AddMeasure(Guid deviceId, double lat, double lng, double measure)
        {
            var measurement = new Measurement()
            {
                DateTime = DateTime.Now,
                WaterLevel = (float)measure + (float)1.33
            };
            
            var existingDevice = _dbContext.Devices.Find(deviceId);
            if (existingDevice == null)
            {
                _dbContext.Devices.Add(new Device
                {
                    DeviceId = deviceId,
                    Position = CreateCoord(45.521758, -73.860316),
                    MeasurementHistory = new List<Measurement> { measurement }
                });
            }
            else
            {
                measurement.Device = existingDevice;
                _dbContext.Measurements.Add(measurement);
            }

            _dbContext.SaveChanges();
        }

        [HttpPost("{deviceId}/Reboot")]
        public async Task<SendCommandResponse> SendRebootCommand(Guid deviceId, int rebootTimeSec)
        {
            await _mqttManager.SendRebootCommand(deviceId, rebootTimeSec);

            var response = new SendCommandResponse
            {
                DeviceId = deviceId,
                CommandId = Guid.NewGuid(),
                Success = true
            };

            return response;
        }

        [HttpPost("{deviceId}/RefreshPosition")]
        public async Task<SendCommandResponse> SendRefreshPositionCommand(Guid deviceId)
        {
            await _mqttManager.SendRefreshPositionCommand(deviceId);

            var response = new SendCommandResponse
            {
                DeviceId = deviceId,
                CommandId = Guid.NewGuid(),
                Success = true
            };

            return response;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HackQcWebApi.Data;
using HackQcWebApi.DBModels;
using HackQcWebApi.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NetTopologySuite.Geometries;

namespace HackQcWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SocialController : ControllerBase
    {
        private DatabaseContext _dbContext;
        [HttpPost("AddEvent")]
        public SocialCommandResponseDto AddSocialEvent(AddSocialEventDto dto)
        {
            Guid Addid = Guid.NewGuid();
            DateTime dateTime = DateTime.Now;
            var socialevent = new Social
            {
                id = Addid,
                EventType = (DBModels.SocialEventType)dto.EventType,
                Description = dto.Description,
                CreatedDateTime = dateTime,
                Position = CreateCoord(dto.Coordinates.Lat, dto.Coordinates.Lng)
            };
            _dbContext.Social.Add(socialevent);
            _dbContext.SaveChanges();

            return new SocialCommandResponseDto
            {
                Success = true,
                guid = Addid
            };
        }
        public SocialController(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        private Point CreateCoord(double y, double x)
        {
            var geoFactory = new GeometryFactory(new PrecisionModel(), 4326);
            return geoFactory.CreatePoint(new Coordinate(x, y));
        }

        [HttpPost("{eventId}/AddComment")]
        public SocialCommandResponseDto AddComment(Guid eventId, AddCommentDto dto)
        {
            DateTime dateTime = DateTime.Now;

            var socialevent = _dbContext.Social.Find(eventId);

            if (socialevent.Comments != null)
            {
                socialevent.Comments.Add(new Comments { Comment = dto.Comment, CreatedDateTime = dateTime });
            }
            else
            {
                socialevent.Comments = new List<Comments>();
                socialevent.Comments.Add(new Comments { Comment = dto.Comment, CreatedDateTime = dateTime });
            }

            _dbContext.SaveChanges();
            return new SocialCommandResponseDto
            {
                Success = true,
                guid = eventId
            };
        }

        [HttpGet("All")]
        public IQueryable<SocialEventDto> GetEvents()
        {
            var SocialList = _dbContext.Social.Select(s => new SocialEventDto
            {
                EventId = s.id,
                Coordinates = new CoordinateDto(s.Position.X, s.Position.Y),
                EventType = (DTOs.SocialEventType)s.EventType,
                Description = s.Description,
                Comments = s.Comments.Select(c => new CommentDto
                {
                    Comment = c.Comment,
                    CommentId = c.CommentId,
                    CreatedDateTime = c.CreatedDateTime
                }),
                Likes = s.Likes,
                CreatedDateTime = s.CreatedDateTime
            });

            return SocialList;
        }

        [HttpPost("{eventId}/Like")]
        public SocialCommandResponseDto LikeEvent(Guid eventId)
        {
            var socialevent = _dbContext.Social.Find(eventId);
            socialevent.Likes++;

            _dbContext.SaveChanges();
            return new SocialCommandResponseDto
            {
                Success = true,
                guid = eventId
            };
        }

        [HttpPost("{eventId}/Dislike")]
        public SocialCommandResponseDto DislikeEvent(Guid eventId)
        {
            return new SocialCommandResponseDto
            {
                Success = true
            };
        }

        [HttpDelete("{eventId}")]
        public SocialCommandResponseDto DeleteEvent(Guid eventId)
        {

            var socialevent = _dbContext.Social.Include(s => s.Comments).Where(s=>s.id == eventId).First();
            
            socialevent.Comments.Clear();
            _dbContext.SaveChanges();

            _dbContext.Social.Remove(socialevent);
            _dbContext.SaveChanges();
            return new SocialCommandResponseDto
            {
                Success = true
            };
        }
    }
}
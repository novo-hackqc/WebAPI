﻿using DotSpatial.Projections;

using HackQcWebApi.Data;
using HackQcWebApi.DBModels;
using HackQcWebApi.DTOs;
using HackQcWebApi.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NetTopologySuite.Geometries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace HackQcWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DashboardController : ControllerBase
    {
        private DatabaseContext _dbContext;
        private SNS _sns;
        public DashboardController(DatabaseContext dbContext, SNS sns)
        {
            _dbContext = dbContext;
            _sns = sns;
        }
        [HttpPost("SendSms")]
        public async Task<DashboardResponseDto> SendSms(SendSmsDto dto)
        {
            Guid SubGuid = Guid.NewGuid();
            Point point = CreateCoord(dto.Coordinates.Lat, dto.Coordinates.Lng);
            var subs = GetSubscriptionsInRange(point, dto.Radius);
            await Task.WhenAll(subs.Select(i => _sns.SendSMS(i.Endpoint, dto.Text)));
            return new DashboardResponseDto
            {
                Success = true
            };
        }
        [HttpGet("Stats")]
        public StatsDto GetStats()
        {
                var geoFactory = new GeometryFactory(new PrecisionModel(), 32198);
            //    var espg32198 = new ProjectionInfo();
            //    var espg4326 = new ProjectionInfo();

            //    espg32198.ParseEsriString(ESRI_EPSG_32198);
            //    espg4326.ParseEsriString(ESRI_EPSG_4326);

            var geographic = ProjectionInfo.FromProj4String(KnownCoordinateSystems.Geographic.World.WGS1984.ToProj4String());
            var projected = ProjectionInfo.FromProj4String(KnownCoordinateSystems.Projected.NorthAmerica.CanadaLambertConformalConic.ToProj4String());



            var stat = new StatsDto();
            var polys = GetPolygons();
            int totalCount = polys.Select(i => GetAddressesCountInPolygon(i)).Sum();
            
            stat.FloodedAddresses = totalCount;
            stat.FloodedPeople = Convert.ToInt32(totalCount * 2.3);
            stat.PeopleRequestingHelp = GetHelpRequestCount();

            foreach (var poly in polys)
            {
                var newCord = new List<Coordinate>();

                var coors = poly.Coordinates;
                foreach (var coor in coors)
                {
                    var xy = new double[] { coor.X, coor.Y };
                    var z = new double[] { 0 };
                    Reproject.ReprojectPoints(xy, z, geographic, projected, 0, 1);
                    newCord.Add(new Coordinate(xy[0], xy[1]));
                }

                var newPoly = geoFactory.CreatePolygon(newCord.ToArray());
                stat.FloodedArea += newPoly.Area;
            }


            stat.FloodedArea /= 1000000;
            return stat; 
        }
        private IEnumerable<Subcription> GetSubscriptionsInRange(Point point, double range)
        {
            var models = _dbContext.Subcription
                .Where(d => d.Position.IsWithinDistance(point, range))
                .ToList();
            return models;
        }
        private Point CreateCoord(double y, double x)
        {
            var geoFactory = new GeometryFactory(new PrecisionModel(), 4326);
            return geoFactory.CreatePoint(new Coordinate(x, y));
        }
        private int GetAddressesCountInPolygon(Polygon poly)
        {
            var model = _dbContext.Addresses
                .Where(d => d.Position.CoveredBy(poly))
                .Count();
            return model;
        }
        private IEnumerable<Polygon> GetPolygons()
        {
            var model = _dbContext.Flaques
                .Where(d => d.Active == true)
                .Select(d => d.Polygon)
                .ToList();
            return model;
        }

        private int GetHelpRequestCount()
        {
            var model = _dbContext.Social
                .Where(d => d.EventType == DBModels.SocialEventType.HelpRequest)
                .Count();
            return model;
        }
    }
}
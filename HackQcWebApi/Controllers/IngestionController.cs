﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HackQcWebApi.Ingestion;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HackQcWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IngestionController : ControllerBase
    {
        private IngestionManager _ingestionManager;

        public IngestionController(IngestionManager ingestionManager)
        {
            _ingestionManager = ingestionManager;
        }

        [HttpPost]
        public void Ingest()
        {
            //_ingestionManager.IngestHist2019();
           // _ingestionManager.IngestMrc();
            //_ingestionManager.IngestZoi();
        }
    }
}
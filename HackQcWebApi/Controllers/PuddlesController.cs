﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HackQcWebApi.Data;
using HackQcWebApi.DBModels;
using HackQcWebApi.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NetTopologySuite.Geometries;

namespace HackQcWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PuddlesController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public PuddlesController(DatabaseContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<PuddleDto>> GetPuddlesAsync([Required]double lat, [Required]double lng, [Required]double range, int previsionHours)
        {
            var geoFactory = new GeometryFactory(new PrecisionModel(), 4326);
            var puddles = new List<PuddleDto>();

            var zones = _context.FloodableZones.Where(z => 
                z.Geometry.Contains(geoFactory.CreatePoint(new Coordinate(lng, lat))))
                .OrderBy(z => z.ZoneId)
                .ToList();

            return puddles;
        }
    }
}
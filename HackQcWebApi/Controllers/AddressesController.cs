﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HackQcWebApi.Data;
using HackQcWebApi.DBModels;
using HackQcWebApi.DTOs;
using HackQcWebApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NetTopologySuite.Geometries;

namespace HackQcWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AddressesController : ControllerBase
    {
        private DatabaseContext _dbContext;
        private SNS _sns;

        public AddressesController(DatabaseContext dbContext, SNS sns)
        {
            _dbContext = dbContext;
            _sns = sns;

        }

        private NetTopologySuite.Geometries.Point CreateCoord(double y, double x)
        {
            var geoFactory = new GeometryFactory(new PrecisionModel(), 4326);
            return geoFactory.CreatePoint(new Coordinate(x, y));
        }

        [HttpGet("Info")]
        public AddressInfoDto GetAddressInfo([Required] double lat, [Required] double lng)
        {
            var geoFactory = new GeometryFactory(new PrecisionModel(), 4326);
            var result = new AddressInfoDto();

            /*var coords = new List<Point>();
            coords.Add(CreateCoord(46.608813, -72.679905));
            coords.Add(CreateCoord(46.608690, -72.678770));
            coords.Add(CreateCoord(46.609742, -72.681475));
            coords.Add(CreateCoord(46.610207, -72.682087));
            coords.Add(CreateCoord(46.605631, -72.675529));
            coords.Add(CreateCoord(46.609453, -72.675057));
            coords.Add(CreateCoord(46.604141, -72.675669));
            coords.Add(CreateCoord(46.607025, -72.681972));
            coords.Add(CreateCoord(46.610478, -72.677915));
            coords.Add(CreateCoord(46.604045, -72.672913));

            var times = new List<long>();

            for (int i = 0; i < coords.Count; i++)
            {
                var stopwatch = Stopwatch.StartNew();
                var isWater = _dbContext.WaterBodies
                    .Any(w => w.Geometry.Contains(coords[i]));
                stopwatch.Stop();
                times.Add(stopwatch.ElapsedMilliseconds);

            }*/

            var zone = _dbContext.FloodableZones.Where(z =>
                    z.Geometry.Contains(geoFactory.CreatePoint(new Coordinate(lng, lat))))
                .OrderBy(z => z.ZoneId)
                .FirstOrDefault();

            if (zone == null)
            {
                result.FloodableZoneType = FloodableZoneType.None;
                result.FloodableZoneDescription = "";
            }
            else
            {
                result.FloodableZoneType = zone.Type;
                result.FloodableZoneDescription = zone.Description;
            }

            var years = _dbContext.FloodHistories
                .Where(h => h.Geometry.Contains(geoFactory.CreatePoint(new Coordinate(lng, lat))))
                .ToList();

            result.FloodedYears = years.Select(y => y.Datetime.Year).Distinct();
            result.CurrentWaterLevel = 0.5;

            return result;
        }

        [HttpPost("Subscribe")]
        public async Task<SubscriptionResultDto> Subscribe(SubscriptionDto dto)
        {
            Guid SubGuid = Guid.NewGuid();
            string arn = await _sns.CreateTopicSubscribe(SubGuid, dto.Address.Description, dto.PhoneNumber);
            var subscription = new Subcription
            {
                Address = dto.Address.Description,
                PhoneNumber = dto.PhoneNumber,
                SubcriptionId = SubGuid,
                Endpoint = arn,
                Position = CreateCoord(dto.Address.Coordinates.Lat, dto.Address.Coordinates.Lng)
            };
            _dbContext.Subcription.Add(subscription);
            _dbContext.SaveChanges();

            var sb = new StringBuilder()
               .Append("Vous êtes maintenant abonné aux alertes de Nov'eau pour l'adresse: ")
               .Append(dto.Address.Description)
               .Append(".\n")
               .Append("Vous pouvez vous désabonner de ces alerte à tout moment en répondant CANCEL.");
            var msg = sb.ToString();

            await _sns.SendSMS(subscription.Endpoint, msg);

            return new SubscriptionResultDto
            {
                Success = true
            };
        }
    }
}
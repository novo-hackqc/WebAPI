﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace HackQcWebApi.Mqtt
{
    public interface ICommand<T>
    {
        public Guid DeviceId { get; set; }
        public CommandId CommandId { get; }
        public IEnumerable<T> Arguments { get; set; }
    }

    public class RebootCommand : ICommand<Int32>
    {
        public Guid DeviceId { get; set; }
        public  CommandId CommandId => CommandId.Reboot;
        public IEnumerable<int> Arguments { get; set; }

        public RebootCommand(Guid deviceId, Int32 rebootTime)
        {
            DeviceId = deviceId;
            Arguments = new List<Int32> { rebootTime };
        }
    }

    public class RefreshPositionCommand : ICommand<int>
    {
        public Guid DeviceId { get; set; }
        public CommandId CommandId => CommandId.RefreshPosition;
        public IEnumerable<int> Arguments { get; set; }
    }

    public enum CommandId
    {
        Reboot = 1,
        SetLed = 2,
        RefreshPosition = 3
    }
}

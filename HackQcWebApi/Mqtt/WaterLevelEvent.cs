﻿using System;

namespace HackQcWebApi.Mqtt
{
    public class WaterLevelEvent
    {
        public Guid DeviceId { get; set; }
        public float WaterLevel { get; set; }
        public float Lat { get; set; }
        public float Lng { get; set; }
    }
}
﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using HackQcWebApi.Data;
using HackQcWebApi.DBModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using MQTTnet;
using MQTTnet.Client.Connecting;
using MQTTnet.Client.Disconnecting;
using MQTTnet.Client.Options;
using MQTTnet.Client.Receiving;
using MQTTnet.Extensions.ManagedClient;
using MQTTnet.Formatter;
using MQTTnet.Protocol;
using NetTopologySuite.Geometries;

namespace HackQcWebApi.Mqtt
{
    public class MqttManager
    {
        private IManagedMqttClient _mqttClient;
        private JsonSerializerOptions _serializerOptions;

        private readonly IServiceScopeFactory _scopeFactory;

        public MqttManager(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
        }

        public async Task<bool> Start()
        {
            var clientCert = new X509Certificate2("Certificate/iot.pfx", "", X509KeyStorageFlags.Exportable);

            var options = new ManagedMqttClientOptionsBuilder()
                .WithAutoReconnectDelay(TimeSpan.FromSeconds(5))
                .WithClientOptions(new MqttClientOptionsBuilder()
                    .WithCleanSession()
                    .WithProtocolVersion(MqttProtocolVersion.V311)
                    .WithClientId($"Server-{Guid.NewGuid().ToString()}")
                    .WithTcpServer("ajsyjv86x5lcf-ats.iot.us-east-1.amazonaws.com", 8883)
                    .WithTls(new MqttClientOptionsBuilderTlsParameters()
                    {
                        IgnoreCertificateChainErrors = true,
                        Certificates = new List<byte[]> {clientCert.Export(X509ContentType.Pfx)},
                        AllowUntrustedCertificates = true,
                        UseTls = true,
                    })
                )
                .Build();

            var mqttClient = new MqttFactory().CreateManagedMqttClient();

            mqttClient.ConnectedHandler = new MqttClientConnectedHandlerDelegate(OnPublisherConnected);
            mqttClient.DisconnectedHandler = new MqttClientDisconnectedHandlerDelegate(OnPublisherDisconnected);

            await mqttClient.StartAsync(options);

            await mqttClient.SubscribeAsync("devices/+/waterLevel", MqttQualityOfServiceLevel.AtLeastOnce);
            mqttClient.ApplicationMessageReceivedHandler = new MqttApplicationMessageReceivedHandlerDelegate(OnMessageReceived);
            
            await mqttClient.PublishAsync(new ManagedMqttApplicationMessageBuilder()
                .WithApplicationMessage(new MqttApplicationMessageBuilder()
                    .WithAtLeastOnceQoS()
                    .WithPayload("Connected from Server")
                    .WithTopic("test_topic")
                    .Build())
                .Build());

            _mqttClient = mqttClient;
            _serializerOptions = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                WriteIndented = false
            };

            return true;
        }

        public async Task SendRebootCommand(Guid deviceId, Int32 rebootTime)
        {
            var command = new RebootCommand(deviceId, rebootTime);
            var commandJson = JsonSerializer.Serialize(command, _serializerOptions);

            await SendCommand(deviceId, commandJson);
        }

        public async Task SendRefreshPositionCommand(Guid deviceId)
        {
            var command = new RefreshPositionCommand
            {
                DeviceId = deviceId
            };
            var commandJson = JsonSerializer.Serialize(command, _serializerOptions);

            await SendCommand(deviceId, commandJson);
        }

        private async Task SendCommand(Guid deviceId, string command)
        {
            await _mqttClient.PublishAsync(new ManagedMqttApplicationMessageBuilder()
            .WithApplicationMessage(new MqttApplicationMessageBuilder()
                .WithAtLeastOnceQoS()
                .WithPayload(command)
                .WithTopic($"devices/{deviceId}/commands")
                .Build())
            .Build());
        }

        private void OnMessageReceived(MqttApplicationMessageReceivedEventArgs e)
        {
            var messageType = e.ApplicationMessage.Topic.Substring(45);
            switch (messageType)
            {
                case "waterLevel":
                    var waterLevelEventString = Encoding.UTF8.GetString(e.ApplicationMessage.Payload, 0, e.ApplicationMessage.Payload.Length-1);
                    OnWaterLevelReceived(JsonSerializer.Deserialize<WaterLevelEvent>(waterLevelEventString, _serializerOptions));
                    break;
            }
        }

        private NetTopologySuite.Geometries.Point CreateCoord(double y, double x)
        {
            var geoFactory = new GeometryFactory(new PrecisionModel(), 4326);
            return geoFactory.CreatePoint(new Coordinate(x, y));
        }

        private void OnWaterLevelReceived(WaterLevelEvent e)
        {
            Console.WriteLine(e);

            var measurement = new Measurement
            {
                DateTime = DateTime.Now,
                WaterLevel = e.WaterLevel + (float)1.33
            };

            using var scope = _scopeFactory.CreateScope();
            var dbContext = scope.ServiceProvider.GetRequiredService<DatabaseContext>();

            var existingDevice = dbContext.Devices.Find(e.DeviceId);
            if (existingDevice == null)
            {
                dbContext.Devices.Add(new Device
                {
                    DeviceId = e.DeviceId,
                    Position = CreateCoord(45.521758, -73.860316),
                    MeasurementHistory = new List<Measurement> {measurement}
                });
            }
            else
            {
                measurement.Device = existingDevice;
                dbContext.Measurements.Add(measurement);
            }

            dbContext.SaveChanges();
        }

        private static void OnPublisherConnected(MqttClientConnectedEventArgs x)
        {
            Console.WriteLine("Connected to MQTT broker");
        }

        private static void OnPublisherDisconnected(MqttClientDisconnectedEventArgs x)
        {
            Console.WriteLine("Disconnected from MQTT broker");
        }

    }
}
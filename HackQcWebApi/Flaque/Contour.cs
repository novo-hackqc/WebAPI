﻿using flaque;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

class Contour
{
    List<MapPoint> points = new List<MapPoint>();
    private double waterLevel;

    public Contour(double waterLevel)
    {
        this.waterLevel = waterLevel;
    }

    public bool Add(MapPoint p)
    {      
        if (!contains(p.point))
        {
            points.Add(p);
            return true;
        }
        return false;
    }

    private bool contains(Point p)
    {
        foreach(MapPoint mp in points)
        {
            if (mp.point == p)
                return true;
        }
        return false;
    }

    public async Task<Contour> increaseContourSizeAsync(MapPointFetcher fetcher)
    {
        List<MapPoint> newMps = new List<MapPoint>();
        foreach (MapPoint mp in points)
        {
            List<MapPoint> voisinsSec = await mp.getVoisinsSec(fetcher);
            foreach (MapPoint voisin in voisinsSec)
            {
                if (await voisin.isBorderAsync(fetcher) && !newMps.Contains(voisin))
                {
                    newMps.Add(voisin);
                }
            }
        }
        points = newMps;
        return this;
    }


    //On ordonne les points dans le bonne ordre pour former le polygone
    public async Task<Polygon> getPolygonAsync(MapPointFetcher fetcher)
    {
        List<MapPoint> pointsLeft = await remInvalidContourPoints(fetcher,points);

        Console.WriteLine("dif: " + (points.Count - pointsLeft.Count));
        List<List<MapPoint>> groups = new List<List<MapPoint>>();
        if (pointsLeft.Count == 0)
            return null;

        groups.Add(new List<MapPoint>());
        MapPoint currentPoint = points[0];
        pointsLeft.RemoveAt(0);

        while (pointsLeft.Count > 0)
        {
            if (currentPoint.point == new Point(-2, -12))
            {

            }
            List<MapPoint> voisin = await currentPoint.getGroupVoisinAsync(fetcher, pointsLeft);
            MapPoint bestVois = voisinSelection(voisin, pointsLeft);
            if (bestVois != null)
            {
                groups[groups.Count - 1].Add(bestVois);
                currentPoint = bestVois;
                pointsLeft.Remove(bestVois);
            }
            else
            {
                groups.Add(new List<MapPoint>());
                currentPoint = points[0];
                pointsLeft.RemoveAt(0);

            }
        }

        List<MapPoint> mainPoly = getMainPolygon(groups);
        groups.Remove(mainPoly);


        return new Polygon(mainPoly, groups);
    }

    private async Task<List<MapPoint>> remInvalidContourPoints(MapPointFetcher fetcher,List<MapPoint> baseList)
    {
        List<MapPoint> mps = new List<MapPoint>();
        bool changed = false;
        foreach (MapPoint mp in baseList)
        {
            //On drop les points qui sont des lignes
            if ((await mp.getAllVoisinsSub(fetcher)).Count > 2)
                mps.Add(mp);
            else
                changed = true;
        }

        if (changed)
            return await remInvalidContourPoints(fetcher, mps);
        else
            return mps;

    }

    public List<MapPoint> getMainPolygon(List<List<MapPoint>> polygons)
    {
        int max = polygons[0].Count;
        List<MapPoint> best = polygons[0];

        for (int i = 0; i < polygons.Count; i++)
        {
            if (polygons[i].Count > max)
            {
                max = polygons[i].Count;
                best = polygons[i];
            }
        }
        return best;
    }


    public static MapPoint voisinSelection(List<MapPoint> voisins, List<MapPoint> pointsLeft)
    {
        if (voisins.Count == 0)
            return null;

        int min = voisins[0].getVoisins(pointsLeft).Count;
        MapPoint bestVois = voisins[0];

        for (int i = 1; i < voisins.Count; i++)
        {
            int count = voisins[i].getVoisins(pointsLeft).Count;
            if (count < min)
            {
                min = count;
                bestVois = voisins[i];
            }
            if (count == min)
            {

            }
        }

        return bestVois;
    }

    public static bool isIn(MapPoint point, List<List<MapPoint>> mpGroups)
    {
        foreach (List<MapPoint> mpGroup in mpGroups)
        {
            foreach (MapPoint mp in mpGroup)
            {
                if (point == mp)
                    return true;
            }
        }
        return false;
    }



}

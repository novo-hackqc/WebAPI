﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using NetTopologySuite.Geometries;

namespace flaque
{


    public class MapPoint
    {

        public static long c = 0;
        public Point point { get; private set; }

        public double elevation { get; private set; }
        private bool cached = false;
        public bool isSubmergedCache = false;
        public Location location { get; private set; }

        public MapPoint(int x, int y, double elevation, double lat, double lng)
        {
            this.point = new Point(x, y);
            this.elevation = elevation;
            this.location = new Location(lat, lng);
        }
        public MapPoint(Point point, Location location, double elevation)
        {
            this.point = point;
            this.elevation = elevation;
            this.location = location;
        }

        public async Task<MapPoint[]> getVoisins(MapPointFetcher fetcher)
        {
            MapPoint[] voisins = new MapPoint[4];
            Point[] pVois = point.getVoisins();
            for (int i = 0; i < voisins.Length; i++)
            {
                voisins[i] = await fetcher.getMapPoint(pVois[i]);
            }
            return voisins;
        }

        public async Task<MapPoint[]> getAllVoisins(MapPointFetcher fetcher)
        {
            MapPoint[] voisins = new MapPoint[8];
            Point[] pVois = point.getAllVoisins();
            for (int i = 0; i < voisins.Length; i++)
            {
                voisins[i] = await fetcher.getMapPoint(pVois[i]);
            }
            return voisins;
        }

        private async Task<List<MapPoint>> getAllVoisinsSec(MapPointFetcher fetcher)
        {
            List<MapPoint> voisinsSec = new List<MapPoint>();
            MapPoint[] voisins = await getAllVoisins(fetcher);
            foreach (MapPoint voisin in voisins)
            {
                if (!voisin.wasSubmerged())
                    voisinsSec.Add(voisin);
            }
            return voisinsSec;
        }

        public async Task<List<MapPoint>> getVoisinsSec(MapPointFetcher fetcher)
        {
            List<MapPoint> voisinsSec = new List<MapPoint>();
            MapPoint[] voisins = await getVoisins(fetcher);
            foreach (MapPoint voisin in voisins)
            {
                if (!voisin.wasSubmerged())
                    voisinsSec.Add(voisin);
            }
            return voisinsSec;
        }

        public async Task<List<MapPoint>> getAllVoisinsSub(MapPointFetcher fetcher)
        {
            List<MapPoint> voisinsSec = new List<MapPoint>();
            MapPoint[] voisins = await getAllVoisins(fetcher);
            foreach (MapPoint voisin in voisins)
            {
                if (voisin.wasSubmerged())
                    voisinsSec.Add(voisin);
            }
            return voisinsSec;
        }

        //Donne tout les voisins parmis les points
        public List<MapPoint> getVoisins(List<MapPoint> others)
        {
            List<MapPoint> voisins = new List<MapPoint>();
            foreach (MapPoint mp in others)
            {
                if (isVoisin(mp))
                {
                    voisins.Add(mp);
                }
            }
            return voisins;
        }

        public async Task<bool> isBorderAsync(MapPointFetcher fetcher)
        {
            return (await getVoisinsSec(fetcher)).Count != 0;
        }

        public async Task<List<MapPoint>> getGroupVoisinAsync(MapPointFetcher fetcher, List<MapPoint> others)
        {
            List<MapPoint> mps = new List<MapPoint>();
            List<MapPoint> voisins = getVoisins(others);

            foreach (MapPoint voisin in voisins)
            {
                if (await haveVoisinSecCommun(fetcher, voisin))
                    mps.Add(voisin);
            }

            return mps;
        }

        public bool isVoisin(MapPoint other)
        {
            return point.areVoisin(other.point);
        }

        public async Task<bool> haveVoisinSecCommun(MapPointFetcher fetcher, MapPoint other)
        {
            List<MapPoint> myVoisin = await this.getAllVoisinsSec(fetcher);
            List<MapPoint> otherVoisin = await other.getAllVoisinsSec(fetcher);

            foreach (MapPoint mp in myVoisin)
            {
                if (otherVoisin.Contains(mp))
                    return true;
            }
            return false;
        }

        public bool isSubmerged(double waterLevel, WaterAccess water)
        {

            if (!cached)
            {
                var stopwatch = Stopwatch.StartNew();
                setIsSubmerged(waterLevel >= elevation && !isWater(water));
                stopwatch.Stop();
                MapPoint.c += stopwatch.ElapsedMilliseconds;

            }
            return isSubmergedCache;
        }

        public bool isWater(WaterAccess water)
        {
            return water.isWater(location);
        }

        //Only works if the fetching was done in a square
        private bool wasSubmerged()
        {

            if (!cached)
                return false;
            return isSubmergedCache;
        }

        public void setIsSubmerged(bool value)
        {
            cached = true;
            isSubmergedCache = value;
        }
    }

    public struct Point
    {


        public int x { get; private set; }
        public int y { get; private set; }

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public Point[] getVoisins()
        {
            return new Point[] { new Point(x + 1, y), new Point(x - 1, y), new Point(x, y + 1), new Point(x, y - 1) };
        }

        public Point[] getAllVoisins()
        {
            return new Point[] { new Point(x - 1, y - 1), new Point(x + 1, y - 1), new Point(x - 1, y + 1), new Point(x + 1, y + 1), new Point(x + 1, y), new Point(x - 1, y), new Point(x, y + 1), new Point(x, y - 1) };
        }

        public bool areVoisin(Point other)
        {
            Point[] myVoisin = this.getAllVoisins();
            foreach (Point p in myVoisin)
            {
                if (p == other) return true;
            }
            return false;
        }

        public static bool operator ==(Point p1, Point p2)
        {
            return p1.Equals(p2);
        }
        public static bool operator !=(Point p1, Point p2)
        {
            return !p1.Equals(p2);
        }

        public static Point operator +(Point p1, Point p2)
        {
            return new Point(p1.x + p2.x, p1.y + p2.y);
        }

        public static Point operator -(Point p1, Point p2)
        {
            return new Point(p1.x - p2.x, p1.y - p2.y);
        }

        public override bool Equals(Object obj)
        {
            //Check for null and compare run-time types.
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Point p = (Point)obj;
                return (x == p.x) && (y == p.y);
            }
        }
        public override int GetHashCode()
        {
            return (x << 2) ^ y;
        }
    }

    public struct Location
    {
        private static GeometryFactory geoFactory = new GeometryFactory(new PrecisionModel(), 4326);
        public double lat { get; private set; }
        public double lng { get; private set; }

        public Location(double lat, double lng)
        {
            this.lat = lat;
            this.lng = lng;
        }

        public override String ToString()
        {
            return lat.ToString("f6", System.Globalization.CultureInfo.InvariantCulture) + "," + lng.ToString("f6", System.Globalization.CultureInfo.InvariantCulture);
        }

        public static Location operator +(Location l1, Location l2)
        {
            return new Location(l1.lat + l2.lat, l1.lng + l2.lng);
        }

        public static Location operator *(Location l1, double mult)
        {
            return new Location(l1.lat * mult, l1.lng * mult);
        }

        public static Location operator -(Location l1, Location l2)
        {
            return new Location(l1.lat - l2.lat, l1.lng - l2.lng);
        }

        public Geometry toDbGeo()
        {
            return geoFactory.CreatePoint(new Coordinate(lng, lat));
        }

        public Location pointBetween(Location other,double x)
        {
            Location diff = other - this;
            return this + diff * x;
        }

    }
}
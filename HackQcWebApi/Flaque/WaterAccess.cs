﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using flaque;
using HackQcWebApi.Data;

public class WaterAccess
{
    private DatabaseContext _dbContext;
    public WaterAccess(DatabaseContext _dbContext)
    {
        this._dbContext = _dbContext;
    }

    public bool isWater(Location loc)
    {
        return _dbContext.WaterBodies
            .Any(w => w.Geometry.Contains(loc.toDbGeo()));
    }

}

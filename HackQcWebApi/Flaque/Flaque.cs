﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using flaque;
using HackQcWebApi.Data;


public class Flaque
{
    private WaterAccess waterAccess;

    public Flaque(DatabaseContext dbContext)
    {
        this.waterAccess = new WaterAccess(dbContext);
    }

    //Retourne un array de points formant un polygon ou chaque point est un sommet et chaque point voisin dans l'array forme un arrête
    public async Task<Polygon> GenFlaque(Location startLoc, double waterLevelRel)
    {
        Console.WriteLine("start falsd");
        var stopwatch = Stopwatch.StartNew();

        MapPointFetcher fetcher = new MapPointFetcher(startLoc);
        MapPoint baseMPoint = await fetcher.getMapPoint(new Point(0, 0));
        double waterLevelAbs = baseMPoint.elevation + waterLevelRel;
        baseMPoint.setIsSubmerged(true);


        //   List<Point> dry = new List<Point>();

        //Les points a traiter (ils sont nécessairement submergé)
        Queue<MapPoint> pav = new Queue<MapPoint>();
        HashSet<Point> ignore = new HashSet<Point>();
        Contour contour = new Contour(waterLevelAbs);

        List<MapPoint> sub = new List<MapPoint>();
        sub.Add(baseMPoint);
        pav.Enqueue(baseMPoint);
        while (pav.Count > 0)
        {
            MapPoint currentPoint = pav.Dequeue();
            foreach (MapPoint voisin in (await currentPoint.getVoisins(fetcher)))
            {
                if (voisin.isWater(waterAccess))
                {
                    if (!ignore.Contains(voisin.point))
                    {
                        contour.Add(voisin);
                        ignore.Add(voisin.point);
                        sub.Add(voisin);
                        await voisin.getAllVoisins(fetcher);//on cache
                        voisin.setIsSubmerged(true);
                    }

                }
                else if (voisin.isSubmerged(waterLevelAbs, waterAccess))
                {
                    if (!ignore.Contains(voisin.point))
                    {
                        ignore.Add(voisin.point);
                        pav.Enqueue(voisin);
                        sub.Add(voisin);
                    }

                }
                else
                {
                    contour.Add(currentPoint);
                }
            }

        }
        stopwatch.Stop();


        /*string test = "[";
        foreach (MapPoint mp in sub)
        {
            test += "{location: new google.maps.LatLng(" + mp.location.ToString() + "), weight: " + (waterLevelAbs - mp.elevation) + " },\n";
        }
        test += "]";*/

        //Console.WriteLine(test);
        var polystopwatch = Stopwatch.StartNew();
        //await contour.increaseContourSizeAsync(fetcher);
        Polygon poly = await contour.getPolygonAsync(fetcher);
        stopwatch.Stop();

        Console.WriteLine("---------------------------------------------- TotalTime: " + (stopwatch.ElapsedMilliseconds) + "   BDAccessTime: " + MapPoint.c + "     polytime: " + polystopwatch.ElapsedMilliseconds); ;
        return poly;

    }


}

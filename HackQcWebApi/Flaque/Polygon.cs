﻿
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace flaque
{

    public class MapPolygon
    {
        public List<MapPoint> mainPoly { get; set; }
        public List<List<MapPoint>> holes { get; set; }
        public void simplify()
        {
            //si des qui se suivent sont sur la meme ligne, on enleve les points du milieu
            MapPoint mp1 = mainPoly[0];
            MapPoint mp2 = mainPoly[1];
        }



        public Polygon toPoly()
        {
            return new Polygon(mainPoly, holes);
        }
    }


    public class Polygon
    {
        public List<Location> mainPoly { get; set; }
        public List<List<Location>> holes { get; set; }

        public Polygon(List<MapPoint> mainPoly, List<List<MapPoint>> holes)
        {
            this.mainPoly = new List<Location>();
            foreach (MapPoint mp in mainPoly)
            {
                this.mainPoly.Add(mp.location);
            }

            this.holes = new List<List<Location>>();
            foreach (List<MapPoint> mps in holes)
            {
                List<Location> locs = new List<Location>();
                this.holes.Add(locs);
                foreach (MapPoint mp in mps)
                {
                    locs.Add(mp.location);
                }
            }
        }

        public Polygon smooth(int level, double tension)
        {
            mainPoly = smooth(level, mainPoly, tension);
            return this;

        }

        public List<Location> smooth(int level, List<Location> oldLocs, double tension)
        {
            if (level <= 0)
                return oldLocs;

            double cutdist = 0.05 + (tension * 0.4);
            List<Location> newLocs = new List<Location>();
            //newLocs.Add(oldLocs[0]);
            for (int i = 0; i < oldLocs.Count; i++)
            {
                Location curr = oldLocs[i];
                Location next = oldLocs[(i + 1) % oldLocs.Count];
                newLocs.Add(curr.pointBetween(next, cutdist));
                newLocs.Add(curr.pointBetween(next, 1 - cutdist));
            }
            //newLocs.Add(oldLocs[oldLocs.Count - 1]);
            return smooth(level - 1, newLocs, tension);

        }

        public void enlargePoints(float amount)
        {
            List<PointF> pf = new List<PointF>();
            foreach(Location loc in mainPoly)
            {
                pf.Add(new PointF((float)loc.lat, (float)loc.lng));
            }
            List<PointF> rep = GetEnlargedPolygon(pf, amount);
            List<Location> locations = new List<Location>();
            foreach (PointF pfa in rep)
            {
                locations.Add(new Location(pfa.X, pfa.Y));
            }

            this.mainPoly = locations;
        }

        // Return points representing an enlarged polygon.
        private List<PointF> GetEnlargedPolygon(
            List<PointF> old_points, float offset)
        {
            List<PointF> enlarged_points = new List<PointF>();
            int num_points = old_points.Count;
            for (int j = 0; j < num_points; j++)
            {
                // Find the new location for point j.
                // Find the points before and after j.
                int i = (j - 1);
                if (i < 0) i += num_points;
                int k = (j + 1) % num_points;

                // Move the points by the offset.
                Vector v1 = new Vector(
                    old_points[j].X - old_points[i].X,
                    old_points[j].Y - old_points[i].Y);
                v1.Normalize();
                v1 *= offset;
                Vector n1 = new Vector(-v1.Y, v1.X);

                PointF pij1 = new PointF(
                    (float)(old_points[i].X + n1.X),
                    (float)(old_points[i].Y + n1.Y));
                PointF pij2 = new PointF(
                    (float)(old_points[j].X + n1.X),
                    (float)(old_points[j].Y + n1.Y));

                Vector v2 = new Vector(
                    old_points[k].X - old_points[j].X,
                    old_points[k].Y - old_points[j].Y);
                v2.Normalize();
                v2 *= offset;
                Vector n2 = new Vector(-v2.Y, v2.X);

                PointF pjk1 = new PointF(
                    (float)(old_points[j].X + n2.X),
                    (float)(old_points[j].Y + n2.Y));
                PointF pjk2 = new PointF(
                    (float)(old_points[k].X + n2.X),
                    (float)(old_points[k].Y + n2.Y));

                // See where the shifted lines ij and jk intersect.
                bool lines_intersect, segments_intersect;
                PointF poi, close1, close2;
                FindIntersection(pij1, pij2, pjk1, pjk2,
                    out lines_intersect, out segments_intersect,
                    out poi, out close1, out close2);             

                enlarged_points.Add(poi);
            }

            return enlarged_points;
        }

        // Find the point of intersection between
        // the lines p1 --> p2 and p3 --> p4.
        private void FindIntersection(
            PointF p1, PointF p2, PointF p3, PointF p4,
            out bool lines_intersect, out bool segments_intersect,
            out PointF intersection,
            out PointF close_p1, out PointF close_p2)
        {
            // Get the segments' parameters.
            float dx12 = p2.X - p1.X;
            float dy12 = p2.Y - p1.Y;
            float dx34 = p4.X - p3.X;
            float dy34 = p4.Y - p3.Y;

            // Solve for t1 and t2
            float denominator = (dy12 * dx34 - dx12 * dy34);

            float t1 =
                ((p1.X - p3.X) * dy34 + (p3.Y - p1.Y) * dx34)
                    / denominator;
            if (float.IsInfinity(t1))
            {
                // The lines are parallel (or close enough to it).
                lines_intersect = false;
                segments_intersect = false;
                intersection = new PointF(float.NaN, float.NaN);
                close_p1 = new PointF(float.NaN, float.NaN);
                close_p2 = new PointF(float.NaN, float.NaN);
                return;
            }
            lines_intersect = true;

            float t2 =
                ((p3.X - p1.X) * dy12 + (p1.Y - p3.Y) * dx12)
                    / -denominator;

            // Find the point of intersection.
            intersection = new PointF(p1.X + dx12 * t1, p1.Y + dy12 * t1);

            // The segments intersect if t1 and t2 are between 0 and 1.
            segments_intersect =
                ((t1 >= 0) && (t1 <= 1) &&
                 (t2 >= 0) && (t2 <= 1));

            // Find the closest points on the segments.
            if (t1 < 0)
            {
                t1 = 0;
            }
            else if (t1 > 1)
            {
                t1 = 1;
            }

            if (t2 < 0)
            {
                t2 = 0;
            }
            else if (t2 > 1)
            {
                t2 = 1;
            }

            close_p1 = new PointF(p1.X + dx12 * t1, p1.Y + dy12 * t1);
            close_p2 = new PointF(p3.X + dx34 * t2, p3.Y + dy34 * t2);
        }
    }

    internal class Vector
    {
        public double X;
        public double Y;

        public Vector(double x, double y)
        {
            X = x;
            Y = y;
        }

        public void Normalize()
        {
            if (X > -0.001 && X < 0.001 && Y > -0.001 && Y < 0.001)
                return;

            var maga = mag();
            X /= maga;
            Y /= maga;
        }

        public double mag()
        {
            return Math.Sqrt(X * X + Y * Y);
        }

        public static Vector operator *(Vector l1, double mult)
        {
            return new Vector(l1.X * mult, l1.Y * mult);
        }

    }
}



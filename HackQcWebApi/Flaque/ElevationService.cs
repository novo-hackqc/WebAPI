﻿using flaque;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

public class MapPointFetcher
{
    private Dictionary<Point, MapPoint> cache = new Dictionary<Point, MapPoint>();

    public Location baseLoc;

    //Max point dans une request est autour de 500 (donc edgeLen ne peut pas être > que 11* distPoint

    private static readonly int distPoint = 500; //ditance en 1/10^-6 degré entre chaque point
    private static readonly int edgeLen = distPoint * 11; //en 1/10^-6 degré

    private static readonly int maxCacheSize = 50000; //5k

    private static readonly int maxFetchCount = 20;

    private int fetchCount = 0;
    public MapPointFetcher(Location baseLoc)
    {
        this.baseLoc = baseLoc;
    }

    //Devrait return le point si il est en cache, sinon on va fetch un groupe
    //de point dans une zone alentour du point désiré (pour pas attendre 150 fois)
    public async Task<MapPoint> getMapPoint(Point p)
    {
        if (cache.Count > maxCacheSize)
            throw new Exception("This is getting tooo big");
        if (!cache.ContainsKey(p))
        {
            fetchCount++;
            if (fetchCount > maxFetchCount)
            {

                GenWall(p);
            }
            else
            {
                await FetchPoints(p);
            }

        }
        return cache[p];
    }

    private async Task FetchPoints(Point arround)
    { 
        if (fetchCount > maxFetchCount)
            throw new Exception("YO ton truc est trop gros men");
        MapPoint[] mapPoints = await MapsAPI.getMapPoints(genSquare(arround));
        foreach (MapPoint mp in mapPoints)
        {
            Cache(mp);
        }
    }

    private void GenWall(Point p)
    {
        Console.WriteLine("Walling it");
        MapPoint mp = new MapPoint(p, GenLoc(p), Double.PositiveInfinity);
        Cache(mp);
    }

    private List<Tuple<Point, Location>> genSquare(Point arround)
    {
        List<Tuple<Point, Location>> points = new List<Tuple<Point, Location>>();
        int len = edgeLen / distPoint;
        for (int i = -len; i < len; i++)
        {
            for (int j = -len; j < len; j++)
            {
                Point nvPoint = new Point(i, j) + arround;
                if (!cache.ContainsKey(nvPoint))
                {
                    points.Add(new Tuple<Point, Location>(nvPoint, GenLoc(nvPoint)));
                }
            }
        }

        return points;
    }


    private Location GenLoc(Point p)
    {
        return baseLoc + new Location(p.x * distPoint * 0.000001, p.y * distPoint * 0.000001);
    }

    private void Cache(MapPoint mp)
    {
        cache.Add(mp.point, mp);
    }
}


﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using flaque;
using HackQcWebApi.Data;
using Newtonsoft.Json;

class MapsAPI
{
    private static readonly HttpClient client = new HttpClient();

    private static readonly string apiKey = "AIzaSyBpodC6O-ykfYe2Xv3dyhXLk1k2uKjq5EM";
    private static readonly string elevUrl = "https://maps.googleapis.com/maps/api/elevation/json?";
    public static async Task<MapPoint[]> getMapPoints(List<Tuple<Point, Location>> tPoints)
    {
        if (tPoints.Count == 0)
            return new MapPoint[0];

        string req = elevUrl + genLocations(tPoints) + "&" + genKey();
        //Console.WriteLine("Request: " + tPoints.Count);

        // Call asynchronous network methods in a try/catch block to handle exceptions.

        //HttpResponseMessage response = await client.GetAsync(req);
        //response.EnsureSuccessStatusCode();
        //string responseBody = await response.Content.ReadAsStringAsync();
        // Above three lines can be replaced with new helper method below
        string responseBody = await client.GetStringAsync(req);
        //  Console.WriteLine(responseBody);
        ApiResult resp = JsonConvert.DeserializeObject<ApiResult>(responseBody);
        return resp.toMapPoints(tPoints);
    }


    private static string genKey()
    {
        return "key=" + apiKey;
    }

    private static string genLocations(List<Tuple<Point, Location>> tPoints)
    {
        StringBuilder sb = new StringBuilder("locations=", 10 + tPoints.Count * 22);
        foreach (Tuple<Point, Location> tPoint in tPoints)
        {
            sb.Append(tPoint.Item2.ToString());
            sb.Append("|");
        }
        sb.Remove(sb.Length - 1, 1);//rem last char
        return sb.ToString();
    }

}

public class ElevRes
{
    public double elevation { get; set; }
    public Location location { get; set; }
    public double resolution { get; set; }

    public MapPoint toMapPoint(Point p)
    {
        return new MapPoint(p, location, elevation);
    }
}

public class ApiResult
{
    public ElevRes[] results { get; set; }
    public string status { get; set; }

    public MapPoint[] toMapPoints(List<Tuple<Point, Location>> tPs)
    {
        MapPoint[] mps = new MapPoint[results.Length];
        for (int i = 0; i < mps.Length; i++)
        {
            mps[i] = results[i].toMapPoint(tPs[i].Item1);
        }
        return mps;
    }
}


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Microsoft.EntityFrameworkCore;
using HackQcWebApi.Data;
using HackQcWebApi.Ingestion;
using HackQcWebApi.Mqtt;
using HackQcWebApi.Services;

namespace HackQcWebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "Noveau API", Version = "V1"});
            });

            services.AddDbContext<DatabaseContext>(options => 
                options.UseNpgsql(
                    Configuration.GetConnectionString("DatabaseContext"), 
                    o => o.UseNetTopologySuite()
                    ));

            services.AddSingleton(typeof(MqttManager), typeof(MqttManager));
            services.AddScoped(typeof(IngestionManager), typeof(IngestionManager));
            services.AddScoped(typeof(SNS), typeof(SNS));
            services.AddScoped(typeof(Flaque), typeof(Flaque));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            UpdateDatabase(app);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder => builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Noveau API V1");
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            
        }

        private static void UpdateDatabase(IApplicationBuilder app)
        {
            using var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope();
            using var context = serviceScope.ServiceProvider.GetService<DatabaseContext>();
            var mqtt = serviceScope.ServiceProvider.GetService<MqttManager>();
            context.Database.Migrate();

            //mqtt.SetDbContext(context);
            mqtt.Start().Wait();
        }
    }
}
